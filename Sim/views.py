import urllib.request
import urllib.parse
import re

def response(word):
    key_word = urllib.parse.quote(word)
    URL = 'http://www.simsimi.com/func/reqN?lc=ch&ft=0.0&req=' + key_word

    headers = {'GET':URL,
              'Accept':'application/json, text/javascript, */*; q=0.01',
                'Accept-Encoding':'gzip,deflate,sdch',
                'Accept-Language':'en-US,en;q=0.8,fr;q=0.6,ja;q=0.4,zh-CN;q=0.2,zh-TW;q=0.2',
                'Connection':'keep-alive',
                'Content-Type':'application/json; charset=utf-8',
                'Cookie':'Filtering=0.0; Filtering=0.0; isFirst=1; isFirst=1; simsimi_uid=50540198; simsimi_uid=50540198; teach_btn_url=talk; teach_btn_url=talk; sid=s%3A6FqtmikDwAq2e2BRTKeNueMO.Wn086Pjq93BEyCfuCeExN%2FbMPsTeqDAS%2FCG%2FlNscYAY; selected_nc=ch; selected_nc=ch; __utma=119922954.1548324691.1396220029.1396316574.1399502581.4; __utmb=119922954.16.9.1399502930002; __utmc=119922954; __utmz=119922954.1399502581.4.4.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided)',
                'Host':'www.simsimi.com',
                'Referer':'http://www.simsimi.com/talk.htm',
                'User-Agent':'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.154 Safari/537.36',
                'X-Requested-With':'XMLHttpRequest' 

               }

    req = urllib.request.Request(URL)
    for key in headers:
        req.add_header(key, headers[key])

    html = urllib.request.urlopen(req).read().decode('utf-8')
    temp = re.findall('(?<=sentence_resp":).*', html)
    return temp[0][1:-2]


