import sqlite3 as lite
import sys
import os

try:
    os.remove('calendar.db')
    con = lite.connect('subjects.db')
    with con:
        cur = con.cursor()
        cur.execute("CREATE TABLE Subject(CODE, TITLE, TYPE, CONTACT, PHONE, EMAIL)")

except FileNotFoundError:
    con = lite.connect('subjects.db')
    with con:
        cur = con.cursor()
        cur.execute("CREATE TABLE Subject(CODE, TITLE, TYPE, CONTACT, PHONE, EMAIL)")
