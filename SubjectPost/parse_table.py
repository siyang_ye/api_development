import urllib.request
import urllib
import re
import os
import sys
from bs4 import BeautifulSoup
import sqlite3 as lite

def get_table_body(link):
    page = urllib.request.urlopen(link)
    soup = BeautifulSoup(page)
    table = soup.find("table")
    table = table.find_all('td')[14:]
    return table

def get_row(i, tbody):
    row = []
    for j in range(6*i,6*i+6,1):
        temp = tbody[j].get_text().replace("'","~")
        row.append(temp)
    return row

def get_column(i,tbody):
    columns = []
    while i < len(tbody):
        column = tbody[i].get_text()
        columns.append(column)
        i += 6
    return columns

def get_number_of_courses(tbody):
    return len(get_column(0, tbody))

def setup_dict(tbody):
    result = {}
    for code in get_column(0,tbody):
        result[code] = []
    return result

def get_all_dict(tbody):
    result = setup_dict(tbody)
    for i in range(get_number_of_courses(tbody)):
        row = get_row(i,tbody)
        result[row[0]] = row[1:]
    return result

def insert_database(database_name, tbody):
    con = lite.connect(database_name)
    course_info = get_all_dict(tbody)
    course_codes = get_column(0, tbody)
    with con:
        cur = con.cursor()
        for code in course_codes:
            course_info_list =  course_info[code]
            command = "INSERT INTO Subject VALUES('{}', '{}', '{}', '{}', '{}', '{}')".format(
                        code, course_info_list[0],course_info_list[1],course_info_list[2],
                        course_info_list[3],course_info_list[4])
            print(command)
            cur.execute(command)
            
def get_info(course_name):
    con = lite.connect('subjects.db')
    with con:
        cur = con.cursor()
        command = "SELECT * FROM Subject WHERE TITLE LIKE '{0}%'".format(course_name)
        cur.execute(command)
        course_info = cur.fetchall()
        if course_info:
            return course_info
        else:
            return ()
        
class Template:
    def __init__(self, course_name, sub_type, contact, phone, email):
        self.course_name = course_name
        self.sub_type = sub_type
        self.contact = contact
        self.phone = phone
        self.email = email
        self.major = ''
        self.minor = ''
        self.specialist = ''
    
    def set_major(self,major):
        self.major = major
        
    def set_minor(self, minor):
        self.minor = minor
        
    def set_specialist(self, specialist):
        self.specialist = specialist
        
    def __repr__(self):
        return ("Course name: " + repr(self.course_name) + '\n' +
                "Major: " + repr(self.major) + '\n' +
                "Minor: " + repr(self.minor) + '\n' +
                "Specialist: " + repr(self.specialist) + '\n' +
                "Type: " + repr(self.sub_type) + '\n' +
                "Contact: " + repr(self.contact) + '\n' +
                "Phone: " + repr(self.phone) + '\n' +
                "Email: " + repr(self.email))
    
    
def get_listing(course_name):
    temp = get_info(course_name)
    total = []
    for info in temp:
        if info[1] not in total:
            total.append(info[1])
    result = ''
    for names in total:
        result += names + '\n'
    return 'There are similar courses offered: \n'+ result
        
    
def response(course_name):
    info = get_info(course_name)
    temp = ''
    if info:
        result = Template(info[0][1], info[0][2], info[0][3], info[0][4], info[0][5])
        for infos in info:
            if infos[1] == result.course_name:
                if 'ASMAJ' in infos[0]:
                    result.set_major(infos[0])
                if 'ASMIN' in infos[0]:
                    result.set_minor(infos[0])
                if 'ASSPE' in infos[0]:
                    result.set_specialist(infos[0])
            else:
                temp = get_listing(course_name)
        return str(result) + '\n'+'\n' + temp
    else:
        return "Can't find"