import urllib.request
import urllib
import re
import os
import sys
from bs4 import BeautifulSoup
import sqlite3 as lite
from weixin_test_server.models import PATH

def get_info(course_name):
    con = lite.connect(PATH + '/SubjectPost/subjects.db')
    with con:
        cur = con.cursor()
        command = "SELECT * FROM Subject WHERE TITLE LIKE '{0}%'".format(course_name)
        cur.execute(command)
        course_info = cur.fetchall()
        if course_info:
            return course_info
        else:
            return ()

def get_info_2(course_name):
    con = lite.connect('subjects.db')
    with con:
        cur = con.cursor()
        command = "SELECT * FROM Subject WHERE TITLE='{0}'".format(course_name)
        cur.execute(command)
        course_info = cur.fetchall()
        if course_info:
            return course_info
        else:
            return ()
        
class Template:
    def __init__(self, course_name, sub_type, contact, phone, email):
        self.course_name = course_name
        self.sub_type = sub_type
        self.contact = contact
        self.phone = phone
        self.email = email
        self.major = ''
        self.minor = ''
        self.specialist = ''
    
    def set_major(self,major):
        self.major = major
        
    def set_minor(self, minor):
        self.minor = minor
        
    def set_specialist(self, specialist):
        self.specialist = specialist
        
    def __repr__(self):
        return ("Course name: " + repr(self.course_name) + '\n' +
                "Major: " + repr(self.major) + '\n' +
                "Minor: " + repr(self.minor) + '\n' +
                "Specialist: " + repr(self.specialist) + '\n' +
                "Type: " + repr(self.sub_type) + '\n' +
                "Contact: " + repr(self.contact) + '\n' +
                "Phone: " + repr(self.phone) + '\n' +
                "Email: " + repr(self.email))
    def __str__(self):
        return ("Course name: " + str(self.course_name) + '\n' +
                "Major: " + str(self.major) + '\n' +
                "Minor: " + str(self.minor) + '\n' +
                "Specialist: " + str(self.specialist) + '\n' +
                "Type: " + str(self.sub_type) + '\n' +
                "Contact: " + str(self.contact) + '\n' +
                "Phone: " + str(self.phone) + '\n' +
                "Email: " + str(self.email))
    
    
def response_2(course_name):
    info = get_info_2(course_name)
    temp = ''
    if info:
        result = Template(info[0][1], info[0][2], info[0][3], info[0][4], info[0][5])
        for infos in info:
            if infos[1] == result.course_name:
                if 'ASMAJ' in infos[0]:
                    result.set_major(infos[0])
                if 'ASMIN' in infos[0]:
                    result.set_minor(infos[0])
                if 'ASSPE' in infos[0]:
                    result.set_specialist(infos[0])
            else:
                temp = get_listing(course_name)
        return str(result) + '\n'+'\n' + temp
    else:
        return ''
    
def response(course_name):
    info = get_info(course_name)
    info_2 = get_info_2(course_name)
    temp = ''
    if info_2:
        temp_2 = response_2(course_name)
        return str(temp_2)
    else:
        if info:
            result = Template(info[0][1], info[0][2], info[0][3], info[0][4], info[0][5])
            for infos in info:
                if infos[1] == result.course_name:
                    if 'ASMAJ' in infos[0]:
                        result.set_major(infos[0])
                    if 'ASMIN' in infos[0]:
                        result.set_minor(infos[0])
                    if 'ASSPE' in infos[0]:
                        result.set_specialist(infos[0])
                else:
                    temp = get_listing(course_name)
            return str(result) + '\n'+'\n' + temp
        else:
            return "Can't find"
