import requests
from bs4 import BeautifulSoup


def youdao(word):
    url = r'http://dict.youdao.com/search?le=eng&q={0}'.format(word.strip())
    r = requests.get(url)
    if r.ok:
        soup = BeautifulSoup(r.content)
        div = soup.find_all('div', class_='trans-container')[:1]
        ul = BeautifulSoup(str(div[0]))
        li = ul.find_all('li')
        if li:
            for mean in li:
                return (mean.text)
        else:
            return ("Can't find this word")

def response(word):
    try:
        return youdao(word)
    except:
        return None

