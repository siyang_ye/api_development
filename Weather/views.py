import urllib.request
from xml.dom import minidom

WEATHER_URL = 'http://xml.weather.yahoo.com/forecastrss?w=%s&u=c'
WEATHER_NS = 'http://xml.weather.yahoo.com/ns/rss/1.0'

def weather_for_zip(location = '4118'):
    url = WEATHER_URL % location
    dom = minidom.parse(urllib.request.urlopen(url))
    forecasts = []
    for node in dom.getElementsByTagNameNS(WEATHER_NS, 'forecast'):
        forecasts.append({
            'date': node.getAttribute('date'),
            'low': node.getAttribute('low'),
            'high': node.getAttribute('high'),
            'condition': node.getAttribute('text')
        })
    return forecasts

class Template:
    def __init__(self,forecast):
        self.condition = forecast['condition']
        self.date = forecast['date']
        self.low = forecast['low']
        self.high = forecast['high']
        
    def __str__(self):
        return ('Date: ' + self.date + '\n' +
                'Condition: ' + self.condition + '\n' +
                'High: ' + self.high + '\n' +
                'Low: ' + self.low + '\n' + '\n')
    
def response(temp):
    forecasts = weather_for_zip()
    result = ''
    for forecast in forecasts:
        result += str(Template(forecast))
    return result.rstrip()
    