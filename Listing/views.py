import sqlite3 as lite
import sys

def response(course_code):
    temp = str(course_code).upper()
    if len(temp) == 3:
        command = "SELECT COURSE FROM Courses WHERE COURSE LIKE '{}%'".format(temp)
        con = lite.connect('weixin_server.models.cur_path/Listing/calendar.db')

        with con:
            cur = con.cursor()
            cur.execute(command)
            course_info = cur.fetchall()
        if course_info:
            result = ''
            for course in course_info:
                if course[0].strip(',') not in result:
                    result += course[0].strip(',') + '\n'
            return result
        else:
            return None
    return None
